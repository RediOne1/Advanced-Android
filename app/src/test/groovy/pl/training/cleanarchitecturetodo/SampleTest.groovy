package pl.training.cleanarchitecturetodo

import spock.lang.Specification

class SampleTest extends Specification {

    def "sample test"() {
        given:
        def x = 1

        when:
        x = 2

        then:
        x == 2
    }
}
